/**
 * ArrayWriter: Sincroniizacao de threads
 * Exemplo que mostra um problema na
 * escrita compartilhada de um vetor
 * por multiplas threads.
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 09/08/2017
 */
package exemplo3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class NataliaExemplo3  {

    
    public static void main(String [] args){
        
        NataliaVetorCompartilhado v = new NataliaVetorCompartilhado(6);
        
        NataliaArrayWriter w1 = new NataliaArrayWriter(1, v);
        NataliaArrayWriter w2 = new NataliaArrayWriter(11, v);
        sub s = new sub(v);
        
        
        
        ExecutorService executor = Executors.newCachedThreadPool();
        
        executor.execute(w1);
        executor.execute(w2);
        
        //A partir daqui nao aceita mais threads
        executor.shutdown();
        
        //Mostra o conteudo do vetorCompartilhado apos as threads finalizarem
        try {
            //Espera 1 minuto para que todas as threads finalizem
            boolean tasksEnded = executor.awaitTermination(1, TimeUnit.MINUTES);
            
            if (tasksEnded)
                System.out.println(v);
            else
                System.out.println("Timeout!");            
        } catch (InterruptedException e){
            e.printStackTrace();
        }
                    
        
    }//fim main
    
}//fim classe
