/**
 * ArrayWriter: Sincroniizacao de threads
 * Exemplo que mostra um problema na
 * escrita compartilhada de um vetor
 * por multiplas threads.
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 09/08/2017
 */
package exemplo3;

import java.util.*;

public class NataliaVetorCompartilhado {

    private final int[] array;
    private int writeIndex=0; //indice do proximo elemento a ser gravado
    private static Random generator = new Random();
    
    public NataliaVetorCompartilhado(int size){
        array = new int[size];
    }//fim do construtor
    
    public synchronized void add(int value){
        
        int position = writeIndex;
        try {
            //Coloca a thread para dormir entre 0 e 500ms
            Thread.sleep(generator.nextInt(500));
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        
        //Insere um valor na posicao do vetor
        array[position] = value;
        System.out.printf("\n%s escreveu %2d na posicao %d", Thread.currentThread().getName(), value,position);
        
        //Incrementa o indice
        writeIndex++;
        System.out.printf("\nProximo indice: %d", writeIndex);
    }
    
    
       public synchronized void sub(int inicio, int value){
       
        try {
            //Coloca a thread para dormir entre 0 e 500ms
            Thread.sleep(generator.nextInt(500));
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        
        //Insere um valor na posicao do vetor
        array[inicio] = value;
        System.out.printf("\n%s escreveu %2d na posicao %d", Thread.currentThread().getName(), value,inicio);
        
        //Incrementa o indice
        writeIndex++;
        System.out.printf("\nProximo indice: %d", writeIndex);
    }
    //Metodo sobrecarregado
    public String toString(){
        return "\nVetor:\n" + Arrays.toString(array);
    }
    
}
